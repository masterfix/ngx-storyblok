## [0.8.2](https://gitlab.com/masterfix/ngx-storyblok/compare/v0.8.1...v0.8.2) (2021-10-15)


### Bug Fixes

* Update README.md ([8cb08ff](https://gitlab.com/masterfix/ngx-storyblok/commit/8cb08ff6d2fdea901f9691d26fe18fd08f197e5b))

## [0.8.1](https://gitlab.com/masterfix/ngx-storyblok/compare/v0.8.0...v0.8.1) (2021-10-05)


### Bug Fixes

* fix the config schematic run during ng add ([537f2e6](https://gitlab.com/masterfix/ngx-storyblok/commit/537f2e69c5068f96b9544442e69b3b233324ca32))

# [0.8.0](https://gitlab.com/masterfix/ngx-storyblok/compare/v0.7.0...v0.8.0) (2021-10-05)


### Features

* add ngx-storyblok:config schematic ([5e5aaff](https://gitlab.com/masterfix/ngx-storyblok/commit/5e5aaffe0de60747cc78828cf6af3fbdd0e9103c))

# [0.7.0](https://gitlab.com/masterfix/ngx-storyblok/compare/v0.6.0...v0.7.0) (2021-10-04)


### Features

* add more metadata to published npm package ([d980bf1](https://gitlab.com/masterfix/ngx-storyblok/commit/d980bf129266d2c9009d403418a23ac5bf6ecee9))

# [0.6.0](https://gitlab.com/masterfix/ngx-storyblok/compare/v0.5.5...v0.6.0) (2021-10-04)


### Bug Fixes

* fix storyblok bridge updates ([03accfc](https://gitlab.com/masterfix/ngx-storyblok/commit/03accfcf83445d15dcc26c3b6c12f623a30653e6))


### Features

* add getSpace() function to content api service ([d3133ec](https://gitlab.com/masterfix/ngx-storyblok/commit/d3133ecbdee209d57a2cb9f65331c312374e3d62))

## [0.5.5](https://gitlab.com/masterfix/ngx-storyblok/compare/v0.5.4...v0.5.5) (2021-10-02)


### Bug Fixes

* generate CHANGELOG.md ([e11965c](https://gitlab.com/masterfix/ngx-storyblok/commit/e11965ce29a7c9895bc3b9dff5e03612ebca2b46))
