import { StoryblokModule } from 'ngx-storyblok';

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CustomFallbackComponent } from './components/custom-fallback/custom-fallback.component';
import { FeatureComponent } from './components/feature/feature.component';
import { GridComponent } from './components/grid/grid.component';
import { PageComponent } from './components/page/page.component';
import { StoryComponent } from './components/story/story.component';
import { storyblokComponentMap } from './components/storyblok-component-map';
import { TeaserComponent } from './components/teaser/teaser.component';

@NgModule({
  declarations: [
    AppComponent,
    CustomFallbackComponent,
    FeatureComponent,
    GridComponent,
    PageComponent,
    StoryComponent,
    TeaserComponent,
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    StoryblokModule.forRoot({
      accessToken: 'iiXObgCHhcitGPNzNOsUbAtt',
      fallbackComponent: CustomFallbackComponent,
      componentMap: storyblokComponentMap,
    }),
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
