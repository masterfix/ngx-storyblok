import {
  ChangeDetectionStrategy,
  Component,
  HostBinding,
  Input,
} from '@angular/core';
import { StoryblokComponent, Blok } from 'ngx-storyblok';

@Component({
  selector: 'app-storyblok-custom-fallback',
  templateUrl: './custom-fallback.component.html',
  styleUrls: ['./custom-fallback.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CustomFallbackComponent implements StoryblokComponent<Blok> {
  @HostBinding('class.blok') blokClass = true;

  @Input()
  blok!: Blok;
}
