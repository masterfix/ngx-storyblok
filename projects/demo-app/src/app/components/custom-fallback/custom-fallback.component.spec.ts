import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomFallbackComponent } from './custom-fallback.component';

describe('CustomFallbackComponent', () => {
  let component: CustomFallbackComponent;
  let fixture: ComponentFixture<CustomFallbackComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CustomFallbackComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomFallbackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
