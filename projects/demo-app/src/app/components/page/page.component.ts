import {
  ChangeDetectionStrategy,
  Component,
  HostBinding,
  Input,
} from '@angular/core';
import { StoryblokComponent } from 'ngx-storyblok';
import { PageBlok } from '../../models/generated/page-blok';

@Component({
  selector: 'app-storyblok-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PageComponent implements StoryblokComponent<PageBlok> {
  @HostBinding('class.blok') blokClass = true;

  @Input()
  blok: PageBlok | undefined;
}
