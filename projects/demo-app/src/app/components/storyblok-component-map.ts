import { StoryblokComponentMap } from 'ngx-storyblok';
import { FeatureComponent } from './feature/feature.component';
import { GridComponent } from './grid/grid.component';
import { PageComponent } from './page/page.component';
import { TeaserComponent } from './teaser/teaser.component';

export const enum storyblokComponentNames {
  PAGE = 'page',
  TEASER = 'teaser',
  GRID = 'grid',
  FEATURE = 'feature',
}

export const storyblokComponentMap: StoryblokComponentMap = {
  [storyblokComponentNames.PAGE]: PageComponent,
  [storyblokComponentNames.TEASER]: TeaserComponent,
  [storyblokComponentNames.GRID]: GridComponent,
  [storyblokComponentNames.FEATURE]: FeatureComponent,
};
