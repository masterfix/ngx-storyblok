import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Blok, Story } from 'ngx-storyblok';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

@Component({
  selector: 'app-story',
  templateUrl: './story.component.html',
  styleUrls: ['./story.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StoryComponent {
  readonly story$: Observable<Story<Blok>>;

  constructor(readonly route: ActivatedRoute) {
    this.story$ = route.data.pipe(
      map((data) => data.story),
      tap((story) =>
        console.log('demo app story component received story:', story),
      ),
    );
  }
}
