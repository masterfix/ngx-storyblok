import {
  ChangeDetectionStrategy,
  Component,
  HostBinding,
  Input,
} from '@angular/core';
import { StoryblokComponent } from 'ngx-storyblok';
import { TeaserBlok } from '../../models/generated/teaser-blok';

@Component({
  selector: 'app-storyblok-teaser',
  templateUrl: './teaser.component.html',
  styleUrls: ['./teaser.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TeaserComponent implements StoryblokComponent<TeaserBlok> {
  @HostBinding('class.blok') blokClass = true;

  @Input()
  blok: TeaserBlok | undefined;
}
