import {
  ChangeDetectionStrategy,
  Component,
  HostBinding,
  Input,
} from '@angular/core';
import { StoryblokComponent } from 'ngx-storyblok';
import { FeatureBlok } from '../../models/generated/feature-blok';

@Component({
  selector: 'app-storyblok-feature',
  templateUrl: './feature.component.html',
  styleUrls: ['./feature.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FeatureComponent implements StoryblokComponent<FeatureBlok> {
  @HostBinding('class.blok') blokClass = true;

  @Input()
  blok: FeatureBlok | undefined;
}
