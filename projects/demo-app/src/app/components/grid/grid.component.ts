import {
  ChangeDetectionStrategy,
  Component,
  HostBinding,
  Input,
} from '@angular/core';
import { StoryblokComponent } from 'ngx-storyblok';
import { GridBlok } from '../../models/generated/grid-blok';

@Component({
  selector: 'app-storyblok-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GridComponent implements StoryblokComponent<GridBlok> {
  @HostBinding('class.blok') blokClass = true;

  @Input()
  blok: GridBlok | undefined;
}
