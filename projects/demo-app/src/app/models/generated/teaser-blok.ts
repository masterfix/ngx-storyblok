import { Blok } from 'ngx-storyblok';

export interface TeaserBlok extends Blok {
  headline: string;
}
