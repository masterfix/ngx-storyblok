import { Blok } from 'ngx-storyblok';

export interface PageBlok extends Blok {
  columns: Blok[];
}
