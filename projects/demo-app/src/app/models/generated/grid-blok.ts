import { Blok } from 'ngx-storyblok';

export interface GridBlok extends Blok {
  columns: Blok[];
}
