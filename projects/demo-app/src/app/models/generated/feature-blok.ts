import { Blok } from 'ngx-storyblok';

export interface FeatureBlok extends Blok {
  name: string;
}
