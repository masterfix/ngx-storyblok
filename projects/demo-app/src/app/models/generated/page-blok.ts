import { Blok } from 'ngx-storyblok';

export interface PageBlok extends Blok {
  body: Blok[];
  background_color: string;
}
