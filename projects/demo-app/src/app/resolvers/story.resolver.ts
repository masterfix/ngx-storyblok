import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Blok, Story, StoryblokContentApiService } from 'ngx-storyblok';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class StoryResolver implements Resolve<Story<Blok>> {
  constructor(
    private readonly storyblokContentApiService: StoryblokContentApiService,
  ) {}

  resolve(route: ActivatedRouteSnapshot): Observable<Story<Blok>> {
    return this.storyblokContentApiService
      .getStory(route.url.join('/'))
      .pipe(tap((story) => console.log('story resolver got story:', story)));
  }
}
