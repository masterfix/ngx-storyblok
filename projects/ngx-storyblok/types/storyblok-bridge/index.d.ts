import { Blok } from '../../src/lib/models/blok';
import { Story } from '../../src/lib/models/story';

declare class StoryblokBridge {
  constructor(config: { resolveRelations?: string[]; customParent?: string });

  on(
    events: Array<'input'>,
    callback: (event: { action: 'input'; story: Story<Blok> }) => void,
  ): void;

  on(
    events: Array<'change'>,
    callback: (event: {
      reload: boolean;
      action: 'change';
      slug: string;
      storyId: number;
      slugChanged: boolean;
    }) => void,
  ): void;

  on(
    events: Array<'published'>,
    callback: (event: {
      reload: boolean;
      action: 'published';
      slug: string;
      storyId: number;
      slugChanged: boolean;
    }) => void,
  ): void;

  on(events: Array<'unpublished'>, callback: () => void): void;

  on(
    events: Array<'enterEditmode'>,
    callback: (event: {
      reload: boolean;
      action: 'enterEditmode';
      storyId: string;
      componentNames: {
        [k: string]: string;
      };
    }) => void,
  ): void;

  on(events: Array<'customEvent'>, callback: () => void): void;
}

declare global {
  interface Window {
    StoryblokBridge?: typeof StoryblokBridge;
  }
}
