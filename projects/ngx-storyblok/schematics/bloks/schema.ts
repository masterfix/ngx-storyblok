export interface Schema {
  name: string;

  // The name of the project.
  project: string;

  // The path to create the service.
  path: string;
}
