import { Rule, Tree, SchematicsException } from '@angular-devkit/schematics';
import { virtualFs, workspaces } from '@angular-devkit/core';
import { ConfigSchema } from './schema';

interface Config {
  $schema: string;
  projects: {
    [k: string]: {
      spaceId: string;
      accessToken: string;
      oauthToken: string;
    };
  };
}

export function generateConfig(options: ConfigSchema): Rule {
  return async (tree: Tree) => {
    const host = createHost(tree);
    const { workspace } = await workspaces.readWorkspace('/', host);

    const project = workspace.projects.get(options.project);
    if (!project) {
      throw new SchematicsException(`Invalid project name: ${options.project}`);
    }

    const configPath = '.storyblok.json';

    if (tree.exists(configPath)) {
      updateConfig(tree, configPath, (config) => {
        config.projects[options.project] = {
          spaceId: options.spaceId,
          accessToken: options.accessToken,
          oauthToken: options.oauthToken,
        };
      });
    } else {
      createConfig(tree, configPath, () => {
        return {
          $schema: './node_modules/ngx-storyblok/config/schema.json',
          projects: {
            [options.project]: {
              spaceId: options.spaceId,
              accessToken: options.accessToken,
              oauthToken: options.oauthToken,
            },
          },
        };
      });
    }
  };
}

function createHost(tree: Tree): workspaces.WorkspaceHost {
  return {
    async readFile(path: string): Promise<string> {
      const data = tree.read(path);
      if (!data) {
        throw new SchematicsException('File not found.');
      }
      return virtualFs.fileBufferToString(data);
    },
    async writeFile(path: string, data: string): Promise<void> {
      return tree.overwrite(path, data);
    },
    async isDirectory(path: string): Promise<boolean> {
      return !tree.exists(path) && tree.getDir(path).subfiles.length > 0;
    },
    async isFile(path: string): Promise<boolean> {
      return tree.exists(path);
    },
  };
}

function updateConfig(
  host: Tree,
  path: string,
  callback: (config: Config) => void,
): Tree {
  const source = host.read(path);
  if (source) {
    const sourceText = source.toString('utf-8');
    const json = JSON.parse(sourceText); // we parse the json
    callback(json); // this will modify the json object
    host.overwrite(path, JSON.stringify(json, null, 2));
  }
  return host;
}

function createConfig(host: Tree, path: string, callback: () => Config): Tree {
  const content = callback();
  host.create(path, JSON.stringify(content, null, 2));
  return host;
}
