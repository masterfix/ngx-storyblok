export interface ConfigSchema {
  project: string;
  spaceId: string;
  accessToken: string;
  oauthToken: string;
}
