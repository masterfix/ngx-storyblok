import { StoryblokNgComponent } from './storyblok-ng-component';

export type StoryblokComponentMap = { [k: string]: StoryblokNgComponent };
