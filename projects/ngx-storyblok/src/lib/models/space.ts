export interface Space {
  id: number;
  name: string;
  domain: string;
  version: number;
  language_codes: string[];
}
