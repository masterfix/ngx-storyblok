import { Blok } from '../models/blok';
import { StoryblokComponent } from '../models/storyblok-component';
import { Type } from '@angular/core';

export type StoryblokNgComponent = Type<StoryblokComponent<Blok>>;
