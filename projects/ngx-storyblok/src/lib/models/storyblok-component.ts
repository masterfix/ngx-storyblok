import { Blok } from './blok';

export interface StoryblokComponent<T extends Blok> {
  blok: T | undefined;
}
