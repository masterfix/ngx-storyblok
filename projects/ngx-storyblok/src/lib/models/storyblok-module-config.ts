import { StoryblokComponentMap } from './storyblok-component-map';
import { StoryblokNgComponent } from './storyblok-ng-component';

export interface StoryblokModuleConfig {
  accessToken: string;
  spaceId?: string;
  fallbackComponent?: StoryblokNgComponent | null;
  componentMap: StoryblokComponentMap;
}
