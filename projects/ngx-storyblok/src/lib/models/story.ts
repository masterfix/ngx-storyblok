import { Blok } from './blok';

export interface Story<T extends Blok> {
  id: number;
  uid: string;
  name: string;
  slug: string;
  full_slug: string;
  lang: string;
  content: T;
}
