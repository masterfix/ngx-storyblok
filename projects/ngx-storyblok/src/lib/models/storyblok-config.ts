import { InjectionToken } from '@angular/core';
import { StoryblokModuleConfig } from './storyblok-module-config';

export const STORYBLOK_CONFIG = new InjectionToken<StoryblokConfig>(
  'storyblok.config',
);

export interface StoryblokConfig extends StoryblokModuleConfig {
  baseUrl: string;
}
