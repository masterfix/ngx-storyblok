import { map } from 'rxjs/operators';
import { APP_INITIALIZER, ModuleWithProviders, NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { StoryblokComponentDirective } from './directives/storyblok-component.directive';
import { StoryblokFallbackComponent } from './components/storyblok-fallback/storyblok-fallback.component';
import { CommonModule } from '@angular/common';
import { StoryblokModuleConfig } from './models/storyblok-module-config';
import { StoryblokConfig, STORYBLOK_CONFIG } from './models/storyblok-config';
import { BridgeService } from './services/bridge.service';
import { from, Observable } from 'rxjs';
import { StoryblokStoryComponentDirective } from './directives/storyblok-story-component.directive';
import { StoryblokContentApiService } from './services/storyblok-content-api.service';

const defaultStoryblokConfig: StoryblokConfig = {
  accessToken: '',
  spaceId: '',
  baseUrl: 'https://api.storyblok.com/v2',
  fallbackComponent: StoryblokFallbackComponent,
  componentMap: {},
};

@NgModule({
  imports: [CommonModule, HttpClientModule],
  declarations: [
    StoryblokComponentDirective,
    StoryblokFallbackComponent,
    StoryblokStoryComponentDirective,
  ],
  exports: [StoryblokComponentDirective, StoryblokStoryComponentDirective],
  providers: [
    {
      provide: APP_INITIALIZER,
      multi: true,
      deps: [BridgeService, StoryblokContentApiService],
      useFactory: (
        bridgeService: BridgeService,
        storyblokContentApiService: StoryblokContentApiService,
      ) => {
        return (): Observable<void> => {
          console.log('app initializer');
          return from(bridgeService.loadBridgeJsWhenInStoryblokIFrame()).pipe(
            map((result) => {
              console.log('initializer result:', result);
              if (result.bridgeLoaded) {
                storyblokContentApiService.setDefaultVersion('draft');
              }
            }),
          );
        };
      },
    },
  ],
})
export class StoryblokModule {
  static forRoot(
    config: StoryblokModuleConfig,
  ): ModuleWithProviders<StoryblokModule> {
    console.log('storyblok forRoot');
    return {
      ngModule: StoryblokModule,
      providers: [
        {
          provide: STORYBLOK_CONFIG,
          useValue: Object.assign(defaultStoryblokConfig, config),
        },
      ],
    };
  }
}
