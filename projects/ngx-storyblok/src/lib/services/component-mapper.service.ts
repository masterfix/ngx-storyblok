import { Inject, Injectable, Type } from '@angular/core';
import { Blok } from '../models/blok';
import { StoryblokComponent } from '../models/storyblok-component';
import { StoryblokConfig, STORYBLOK_CONFIG } from '../models/storyblok-config';

@Injectable({
  providedIn: 'root',
})
export class ComponentMapperService {
  constructor(
    @Inject(STORYBLOK_CONFIG) private readonly storyblokConfig: StoryblokConfig,
  ) {}

  mapComponent(
    storyblokComponent: string,
    angularComponent: Type<StoryblokComponent<Blok>>,
  ): void {
    this.storyblokConfig.componentMap[storyblokComponent] = angularComponent;
  }

  getMappedAngularComponent(
    storyblokComponent: string,
  ): Type<StoryblokComponent<Blok>> | null {
    const component = this.storyblokConfig.componentMap[storyblokComponent];
    if (component) {
      return component;
    }
    if (this.storyblokConfig.fallbackComponent) {
      return this.storyblokConfig.fallbackComponent;
    }
    return null;
  }
}
