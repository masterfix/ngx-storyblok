import { TestBed } from '@angular/core/testing';
import { StoryblokConfig, STORYBLOK_CONFIG } from '../models/storyblok-config';
import { DynamicComponentService } from './dynamic-component.service';

describe('DynamicComponentService', () => {
  let service: DynamicComponentService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: STORYBLOK_CONFIG, useValue: {} as StoryblokConfig },
      ],
    });
    service = TestBed.inject(DynamicComponentService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
