import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, shareReplay, switchMap, tap } from 'rxjs/operators';
import { Blok } from '../models/blok';
import { Space } from '../models/space';
import { Story } from '../models/story';
import { StoryblokConfig, STORYBLOK_CONFIG } from '../models/storyblok-config';

@Injectable({
  providedIn: 'root',
})
export class StoryblokContentApiService {
  private readonly space$: Observable<Space>;

  private version: 'draft' | 'published' = 'published';

  constructor(
    private readonly http: HttpClient,
    @Inject(STORYBLOK_CONFIG) private readonly storyblokConfig: StoryblokConfig,
  ) {
    this.space$ = this.setupSpaceObservable();
  }

  getSpace(): Observable<Space> {
    return this.space$;
  }

  getStory<T extends Blok>(
    slug: string,
    version?: 'draft' | 'published',
  ): Observable<Story<T>> {
    return this.space$.pipe(
      switchMap((space) => {
        console.log('get story with slug:', slug);
        return this.http.get<{ story: Story<T> }>(this.buildStoryUrl(slug), {
          params: {
            token: this.storyblokConfig.accessToken,
            cv: `${space.version}`,
            version: version || this.version,
          },
        });
      }),
      map((story) => story.story),
    );
  }

  getDefaultVersion(): 'draft' | 'published' {
    return this.version;
  }

  setDefaultVersion(version: 'draft' | 'published'): void {
    this.version = version;
  }

  private setupSpaceObservable(): Observable<Space> {
    return this.http
      .get<{ space: Space }>(`${this.storyblokConfig.baseUrl}/cdn/spaces/me`, {
        params: {
          token: this.storyblokConfig.accessToken,
          cv: `${Date.now()}`,
        },
      })
      .pipe(
        map((result) => result.space),
        tap((space) => console.log('loaded space:', space)),
        shareReplay(1),
      );
  }

  private buildStoryUrl(slug: string): string {
    const sanitzedSlug = slug.substr(0, 1) === '/' ? slug.substr(1) : slug;
    return `${this.storyblokConfig.baseUrl}/cdn/stories/${sanitzedSlug}`;
  }
}
