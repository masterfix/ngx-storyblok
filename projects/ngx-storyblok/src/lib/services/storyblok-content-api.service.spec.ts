/* import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { StoryblokModule } from '../storyblok.module';
import { StoryblokContentApiService } from './storyblok-content-api.service';
import { HttpClientModule, HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Story } from '../models/story';
import { Blok } from '../models/blok';

describe('StoryblokContentApiService', () => {

  let service: StoryblokContentApiService;

  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        StoryblokModule,
      ],
    });
    service = TestBed.inject(StoryblokContentApiService);
    httpClient = TestBed.inject(HttpClient);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return correct story for slug', done => {

    const slug = 'bla';

    service.getStory(slug).subscribe(result => {
      expect(result.slug).toEqual(slug);
      done();
    }, error => {
      console.log('got error');
    });

    const req = httpTestingController.expectOne(slug);

    expect(req.request.method).toEqual('GET');

    req.flush({
      story: {
        slug: slug,
      },
    });

    httpTestingController.verify();
  });

});
 */
