import { DOCUMENT, isPlatformBrowser } from '@angular/common';
import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { Blok } from '../models/blok';
import { Story } from '../models/story';

const STORYBLOK_SCRIPT_ID = 'storyblokBridge';
const STORYBLOK_SCRIPT_SRC = '//app.storyblok.com/f/storyblok-v2-latest.js';

@Injectable({
  providedIn: 'root',
})
export class BridgeService {
  private readonly bridgeStorySubject: Subject<Story<Blok>>;

  readonly bridgeStory$: Observable<Story<Blok>>;

  private _bridgeLoaded = false;

  get bridgeLoaded(): boolean {
    return this._bridgeLoaded;
  }

  constructor(
    @Inject(PLATFORM_ID) private readonly platformId: string,
    @Inject(DOCUMENT) private readonly document: Document,
  ) {
    this.bridgeStorySubject = new Subject<Story<Blok>>();
    this.bridgeStory$ = this.bridgeStorySubject.asObservable();
  }

  private get insideOfStoryblok(): boolean {
    if (!isPlatformBrowser(this.platformId)) {
      return false;
    }
    return window.location.search.includes('_storyblok');
  }

  loadBridgeJsWhenInStoryblokIFrame(): Promise<{ bridgeLoaded: boolean }> {
    return new Promise<{ bridgeLoaded: boolean }>((resolve) => {
      if (!this.insideOfStoryblok) {
        resolve({ bridgeLoaded: false });
        return;
      }

      if (this.document.getElementById(STORYBLOK_SCRIPT_ID)) {
        resolve({ bridgeLoaded: true });
        return;
      }

      const script = this.document.createElement('script');
      script.src = STORYBLOK_SCRIPT_SRC;
      script.id = STORYBLOK_SCRIPT_ID;
      script.onload = () => {
        this.setupBridge();

        this._bridgeLoaded = true;
        resolve({ bridgeLoaded: true });
      };
      this.document.body.appendChild(script);
    });
  }

  private setupBridge() {
    const { StoryblokBridge, location } = window as Window;

    if (!StoryblokBridge) {
      throw new Error('Storyblok bridge is not loaded!');
    }

    const storyblokInstance = new StoryblokBridge({});

    storyblokInstance.on(['published'], () => {
      // reload page if publish is clicked
      location.reload();
    });

    storyblokInstance.on(['change'], () => {
      // reload page if save is clicked
      location.reload();
    });

    storyblokInstance.on(['input'], (event) => {
      // Access currently changed but not yet saved content via:
      this.bridgeStorySubject.next(event.story);
    });
  }
}
