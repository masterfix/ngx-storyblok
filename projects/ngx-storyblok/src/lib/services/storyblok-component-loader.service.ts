import {
  ComponentFactoryResolver,
  ComponentRef,
  Injectable,
  ViewContainerRef,
} from '@angular/core';
import { StoryblokComponent } from '../models/storyblok-component';
import { Blok } from '../models/blok';
import { ComponentMapperService } from './component-mapper.service';

@Injectable({
  providedIn: 'root',
})
export class StoryblokComponentLoaderService {
  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    private componentMapper: ComponentMapperService,
  ) {}

  loadStoryblokComponent(
    viewContainerRef: ViewContainerRef,
    storyblokComponent: string,
  ): ComponentRef<StoryblokComponent<Blok>> | null {
    const component =
      this.componentMapper.getMappedAngularComponent(storyblokComponent);

    if (component === null) {
      return null;
    }

    const componentFactory =
      this.componentFactoryResolver.resolveComponentFactory(component);
    const componentRef =
      viewContainerRef.createComponent<StoryblokComponent<Blok>>(
        componentFactory,
      );

    return componentRef;
  }
}
