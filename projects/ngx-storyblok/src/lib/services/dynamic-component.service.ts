import { StoryblokComponent } from './../models/storyblok-component';
import {
  Injectable,
  Renderer2,
  ViewContainerRef,
  ComponentRef,
} from '@angular/core';
import { Blok } from '../models/blok';
import { StoryblokComponentLoaderService } from './storyblok-component-loader.service';

@Injectable({
  providedIn: 'root',
})
export class DynamicComponentService {
  constructor(
    private storyblokComponentLoader: StoryblokComponentLoaderService,
  ) {}

  loadComponent(
    storyblokComponent: Blok,
    viewContainerRef: ViewContainerRef,
    renderer: Renderer2,
  ): ComponentRef<StoryblokComponent<Blok>> | null {
    console.log(
      'loading dynamic storyblok component:',
      storyblokComponent.component,
    );

    if (viewContainerRef.length > 0) {
      viewContainerRef.clear();
    }

    const componentRef = this.storyblokComponentLoader.loadStoryblokComponent(
      viewContainerRef,
      storyblokComponent.component,
    );

    if (componentRef === null) {
      console.log(
        `skip loading fallback for '${storyblokComponent.component}' component`,
      );
      return null;
    }

    componentRef.instance.blok = storyblokComponent;

    if (storyblokComponent._editable) {
      const editableString = storyblokComponent._editable
        .replace('<!--#storyblok#', '')
        .replace('-->', '');
      const options: { id: string; uid: string; name: string; space: string } =
        JSON.parse(editableString);
      renderer.setAttribute(
        componentRef.location.nativeElement,
        'data-blok-c',
        JSON.stringify(options),
      );
      renderer.setAttribute(
        componentRef.location.nativeElement,
        'data-blok-uid',
        `${options.id}-${options.uid}`,
      );
    }

    return componentRef;
  }
}
