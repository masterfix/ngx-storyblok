import { TestBed } from '@angular/core/testing';
import { StoryblokConfig, STORYBLOK_CONFIG } from '../models/storyblok-config';
import { ComponentMapperService } from './component-mapper.service';

describe('ComponentMapperService', () => {
  let service: ComponentMapperService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {
          provide: STORYBLOK_CONFIG,
          useValue: {
            componentMap: {},
          } as StoryblokConfig,
        },
      ],
    });
    service = TestBed.inject(ComponentMapperService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return for unmapped component', () => {
    expect(service.getMappedAngularComponent('bla')).toBeNull();
  });
});
