import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Blok } from '../../models/blok';
import { StoryblokComponent } from '../../models/storyblok-component';

@Component({
  selector: 'storyblok-fallback',
  templateUrl: './storyblok-fallback.component.html',
  styleUrls: ['./storyblok-fallback.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StoryblokFallbackComponent implements StoryblokComponent<Blok> {
  blok: Blok | undefined;
}
