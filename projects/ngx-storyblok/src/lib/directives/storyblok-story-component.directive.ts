import { BridgeService } from './../services/bridge.service';
import { DynamicComponentService } from './../services/dynamic-component.service';
import {
  Directive,
  Input,
  OnChanges,
  OnDestroy,
  Renderer2,
  ViewContainerRef,
  OnInit,
  ChangeDetectorRef,
  ComponentRef,
} from '@angular/core';
import { Blok } from '../models/blok';
import { Story } from '../models/story';
import { Subscription } from 'rxjs';
import { StoryblokComponent } from '../models/storyblok-component';

@Directive({
  selector: '[storyblokStoryComponent]',
})
export class StoryblokStoryComponentDirective
  implements OnChanges, OnInit, OnDestroy
{
  @Input() storyblokStoryComponent?: Story<Blok>;

  private bridgeStoryUpdateSubscription?: Subscription;

  private loadedComponent?: ComponentRef<StoryblokComponent<Blok>>;

  constructor(
    private readonly viewContainerRef: ViewContainerRef,
    private readonly renderer: Renderer2,
    private readonly dynamicComponentService: DynamicComponentService,
    private readonly bridgeService: BridgeService,
    private readonly changeDetector: ChangeDetectorRef,
  ) {}

  ngOnInit(): void {
    if (this.bridgeService.bridgeLoaded) {
      this.subscribeToBridgeStoryUpdates();
    }
  }

  ngOnChanges(): void {
    console.log('story component directive: onChanges');
    this.loadComponent();
  }

  ngOnDestroy(): void {
    this.unsubscribeFromBridgeStoryUpdates();
  }

  private loadComponent() {
    if (this.storyblokStoryComponent) {
      const component = this.dynamicComponentService.loadComponent(
        this.storyblokStoryComponent.content,
        this.viewContainerRef,
        this.renderer,
      );
      if (component) {
        this.loadedComponent = component;
        console.log('component sucessfully loaded:', this.loadedComponent);
      }
    }
  }

  private subscribeToBridgeStoryUpdates() {
    this.bridgeStoryUpdateSubscription =
      this.bridgeService.bridgeStory$.subscribe((story) => {
        if (this.loadedComponent) {
          this.loadedComponent.instance.blok = story.content;
          this.loadedComponent.injector.get(ChangeDetectorRef).markForCheck();
        }
      });
  }

  private unsubscribeFromBridgeStoryUpdates() {
    if (this.bridgeStoryUpdateSubscription) {
      this.bridgeStoryUpdateSubscription.unsubscribe();
    }
  }
}
