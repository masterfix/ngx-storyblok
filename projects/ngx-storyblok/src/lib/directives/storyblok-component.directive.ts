import { DynamicComponentService } from './../services/dynamic-component.service';
import {
  Directive,
  Input,
  OnChanges,
  Renderer2,
  ViewContainerRef,
} from '@angular/core';
import { Blok } from '../models/blok';

@Directive({
  selector: '[storyblokComponent]',
})
export class StoryblokComponentDirective implements OnChanges {
  @Input()
  storyblokComponent?: Blok;

  constructor(
    private readonly viewContainerRef: ViewContainerRef,
    private readonly renderer: Renderer2,
    private readonly dynamicComponentService: DynamicComponentService,
  ) {}

  ngOnChanges(): void {
    this.loadComponent();
  }

  private loadComponent() {
    if (this.storyblokComponent) {
      this.dynamicComponentService.loadComponent(
        this.storyblokComponent,
        this.viewContainerRef,
        this.renderer,
      );
    }
  }
}
