var __assign =
  (this && this.__assign) ||
  function () {
    __assign =
      Object.assign ||
      function (t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
          s = arguments[i];
          for (var p in s)
            if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
      };
    return __assign.apply(this, arguments);
  };
!(function (e) {
  'function' == typeof define && define.amd ? define(e) : e();
})(function () {
  var e = function (e) {
      return document.querySelector('[data-blok-uid="' + e + '"]');
    },
    t = function (e) {
      var t = e.getBoundingClientRect();
      return (
        (t.top < 0 && t.top + t.height >= 0) ||
        (t.top >= 0 &&
          t.top <=
            (window.innerHeight || document.documentElement.clientHeight))
      );
    },
    o = function (e) {
      var t = {};
      try {
        var o_1 = e.replace(/\\/g, '');
        t = JSON.parse(o_1);
      } catch (t) {
        console.error('Error parsing json', e);
      }
      return t;
    },
    n = function (e) {
      var t = '';
      return (
        location.search
          .substr(1)
          .split('&')
          .forEach(function (o) {
            var n = o.split('=');
            n[0] === e && (t = decodeURIComponent(n[1]));
          }),
        t
      );
    },
    i = function (e) {
      return e && e.display_name
        ? e.display_name
        : 'string' != typeof (t = e.name)
        ? ''
        : t
            .toString()
            .replace(/([A-Z])/g, ' $1')
            .trim()
            .toLowerCase()
            .replace(/[_-]/g, ' ')
            .replace(/(?:^|\s)\S/g, function (e, t) {
              return e.toUpperCase();
            });
      var t;
    },
    s = function (e) {
      var t = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
      t.setAttribute('viewBox', e.viewBox), t.setAttribute('class', e.svgClass);
      var o = document.createElementNS(t.namespaceURI, 'path');
      return (
        o.setAttribute('d', e.path),
        o.setAttribute('fill', e.pathFill),
        o.setAttribute(
          'transform',
          e.pathTransform ? e.pathTransform : 'translate(0 0)',
        ),
        t.appendChild(o),
        t
      );
    };
  var r =
    '@-webkit-keyframes smoke{0%{background-color:rgba(89,197,198,0)}to{background-color:rgba(89,197,198,.5)}}@keyframes smoke{0%{background-color:rgba(89,197,198,0)}to{background-color:rgba(89,197,198,.5)}}.storyblok--outlined .storyblok__outline,.storyblok--outlined [data-blok-c]{outline:1px dashed #b6babf}.storyblok--outlined .storyblok__outline[data-blok-focused=true],.storyblok--outlined [data-blok-c][data-blok-focused=true]{outline:0}.storyblok__hint{box-sizing:border-box;z-index:16777272}.storyblok__highlight,.storyblok__hint{outline:1px solid #00b3b0;pointer-events:none;position:absolute;transition:opacity .2s ease}.storyblok__highlight{background:rgba(89,197,198,.2);z-index:16777270}.storyblok__overlay{box-shadow:0 0 8px 2px rgba(34,42,69,.07);box-sizing:border-box;outline:1px solid #00b3b0;pointer-events:none;position:absolute;z-index:1}.storyblok__overlay-menu{background-color:#00b3b0;border-radius:5px;display:inline-flex;font-family:-apple-system,blinkmacsystemfont,Segoe UI,roboto,helvetica,arial,sans-serif,Apple Color Emoji,Segoe UI Emoji,Segoe UI Symbol;height:30px;left:-1px;pointer-events:auto;position:absolute;top:-40px}.storyblok__overlay-menu--simple .storyblok__overlay-menu-btn{border-bottom-right-radius:5px;border-top-right-radius:5px}.storyblok__overlay-menu-label{color:#fff;font-size:14px;line-height:30px;margin-right:20px;max-width:200px;overflow-x:hidden;text-overflow:ellipsis;white-space:nowrap}.storyblok__overlay-menu-label:first-child{margin-left:20px}.storyblok__overlay-menu-btn{align-items:center;background-color:transparent;border:0;display:flex;justify-content:center;outline:none;padding:0}.storyblok__overlay-menu-btn:hover{background-color:#40c6c4}.storyblok__overlay-menu-btn-action{border-bottom-right-radius:5px;border-left:1px solid #fff;border-top-right-radius:5px;height:inherit;margin:auto 0 auto auto}.storyblok__overlay-menu-btn-parent{border-bottom-left-radius:5px;border-top-left-radius:5px;cursor:pointer;margin:0;width:auto}.storyblok__overlay-menu-svg{box-sizing:border-box;cursor:pointer;height:24px;margin:0 3px;width:24px}.storyblok__overlay--bottom .storyblok__overlay-menu{bottom:-40px;top:auto}.storyblok__overlay--clicked{-webkit-animation-duration:.2s;animation-duration:.2s;-webkit-animation-iteration-count:1;animation-iteration-count:1;-webkit-animation-name:smoke;animation-name:smoke}.storyblok__actions-menu,.storyblok__breadcrumbs-menu{background-color:#fff;border:1px solid #dfe3e8;border-radius:5px;box-shadow:0 2px 17px 3px rgba(34,42,69,.07);box-sizing:content-box;display:none;height:-webkit-fit-content;height:-moz-fit-content;height:fit-content;min-width:156px;overflow:hidden;padding:11px 0;position:absolute;top:25px;z-index:2}.storyblok__actions-menu hr,.storyblok__breadcrumbs-menu hr{background-color:#dfe3e8;border:0;height:1px;margin:11px 0 11px 20px}.storyblok__actions-menu__menu-item,.storyblok__breadcrumbs-menu__menu-item{align-items:center;-webkit-appearance:none;-moz-appearance:none;appearance:none;background:#fff;border:0;color:#1b243f;cursor:pointer;display:flex;font-family:-apple-system,blinkmacsystemfont,Segoe UI,roboto,helvetica,arial,sans-serif,Apple Color Emoji,Segoe UI Emoji,Segoe UI Symbol;font-size:14px;padding:8px 20px;text-align:left;width:100%}.storyblok__actions-menu__menu-item--delete,.storyblok__breadcrumbs-menu__menu-item--delete{color:#ff6159;margin-bottom:0}.storyblok__actions-menu__menu-item--selected,.storyblok__breadcrumbs-menu__menu-item--selected{color:#00b3b0;cursor:default;margin-bottom:0}.storyblok__actions-menu__menu-item--selected:hover,.storyblok__breadcrumbs-menu__menu-item--selected:hover{background:transparent}.storyblok__actions-menu__menu-item:hover,.storyblok__breadcrumbs-menu__menu-item:hover{background:#eff1f3}.storyblok__actions-menu__menu-item:focus,.storyblok__breadcrumbs-menu__menu-item:focus{outline:none}.storyblok__actions-menu{left:calc(100% - 20px)}.storyblok__breadcrumbs-menu{left:-10px}';
  var l = 'storyblok--outlined',
    a = 'storyblok-bridge-stylesheet',
    d = 'storyblok__overlay',
    c = 'storyblok__overlay-menu',
    h = 'storyblok__actions-menu',
    u = 'storyblok__breadcrumbs-menu';
  var m = /** @class */ (function () {
    function m(e) {
      (this.inEditor = !0),
        (this.initialized = !1),
        (this.currentUid = null),
        (this.storyId = ''),
        (this.componentNames = {}),
        (this.outlineOnMoveInterval = null),
        (this.calcInterval = null),
        (this.canAddBlocks = !1),
        (this.canMoveForward = !1),
        (this.navigationBreadcrumbs = []),
        (this.focusState = !1),
        (this.actionsEnabled = !1),
        (this.hinter = null),
        (this.highlighter = null),
        (this.overlay = null),
        (this.componentBase = null),
        (this.componentLabel = null),
        (this.breadcrumbsButtonMenu = null),
        (this.breadcrumbsMenu = null),
        (this.actionsMenu = null),
        (this.actionsMenuButton = null),
        (this.actionsMenuItems = null),
        (this.events = {
          input: [],
          change: [],
          published: [],
          unpublished: [],
          viewLiveVersion: [],
          enterEditmode: [],
          enterComponent: [],
          hoverComponent: [],
          highlightComponent: [],
          customEvent: [],
          pingBack: [],
          sessionReceived: [],
          editedBlok: [],
          deselectBlok: [],
          addedBlock: [],
          deletedBlock: [],
          movedBlock: [],
          duplicatedBlock: [],
        }),
        (this.config = __assign(
          { customParent: null, resolveRelations: null },
          e,
        )),
        this.init();
    }
    Object.defineProperty(m.prototype, 'isInIframe', {
      get: function () {
        return window.top !== window.self;
      },
      enumerable: false,
      configurable: true,
    });
    Object.defineProperty(m.prototype, 'csProtocol', {
      get: function () {
        var e = location.protocol.replace(':', '');
        return 'http' !== e && 'https' !== e && (e = 'https'), e;
      },
      enumerable: false,
      configurable: true,
    });
    Object.defineProperty(m.prototype, 'targetOrigin', {
      get: function () {
        return this.config.customParent
          ? this.config.customParent
          : 'stage' === n('_storyblok_env')
          ? this.csProtocol + '://app-beta.storyblok.com'
          : this.csProtocol + '://app.storyblok.com';
      },
      enumerable: false,
      configurable: true,
    });
    Object.defineProperty(m.prototype, 'lastBreadcrumbItem', {
      get: function () {
        return (
          this.navigationBreadcrumbs[this.navigationBreadcrumbs.length - 1] ||
          {}
        );
      },
      enumerable: false,
      configurable: true,
    });
    m.prototype.isInEditor = function () {
      return this.inEditor;
    };
    m.prototype.init = function () {
      null !== document.body
        ? this.isInIframe &&
          (this.resetAllEvents(),
          this.addMessageListener(),
          this.outlineOnMove(),
          this.buildBridgeStyles(),
          this.on('enterEditmode', this.enterEditmode),
          this.isInIframe &&
            this.sendDataToEditor({
              action: 'initialized',
              config: this.config,
            }),
          (this.initialized = !0))
        : console.error(
            'Body tag not found. Please install the Storyblok bridge script inside the body tag',
          );
    };
    m.prototype.keyPress = function (e, t) {
      (('z' === t.key && t.metaKey) || ('z' === t.key && t.ctrlKey)) &&
        (t.preventDefault(),
        t.stopImmediatePropagation(),
        e.sendDataToEditor({ action: 'undo' })),
        (('y' === t.key && t.metaKey) || ('y' === t.key && t.ctrlKey)) &&
          (t.preventDefault(),
          t.stopImmediatePropagation(),
          e.sendDataToEditor({ action: 'redo' }));
    };
    m.prototype.sendDataToEditor = function (e) {
      window.parent.postMessage(e, this.targetOrigin);
    };
    m.prototype.buildBridgeStyles = function () {
      document.getElementById(a)
        ? ((this.hinter = document.querySelector('.storyblok__hint')),
          (this.highlighter = document.querySelector('.storyblok__highlight')),
          (this.overlay = document.querySelector('.storyblok__overlay')),
          (this.componentBase = document.querySelector(
            '.storyblok__overlay-menu',
          )),
          (this.breadcrumbsButtonMenu = document.querySelector(
            '.storyblok__overlay-menu-btn-parent',
          )),
          (this.breadcrumbsMenu = document.querySelector(
            '.storyblok__breadcrumbs-menu',
          )),
          (this.actionsMenuButton = document.querySelector(
            '.storyblok__overlay-menu-btn-action',
          )),
          (this.actionsMenu = document.querySelector(
            '.storyblok__actions-menu',
          )),
          (this.componentLabel = document.querySelector(
            '.storyblok__overlay-menu > .storyblok__overlay-menu-label',
          )),
          this.createActionsMenuItems())
        : (this.createBridgeStylesheet(),
          this.createHinter(),
          this.createHighlighter(),
          this.createOverlay(),
          this.createComponentContext());
    };
    m.prototype.createBridgeStylesheet = function () {
      var e = document.createElement('style');
      e.setAttribute('type', 'text/css'),
        (e.id = a),
        'textContent' in e ? (e.textContent = r) : (e.styleSheet.cssText = r),
        document.getElementsByTagName('head')[0].appendChild(e);
    };
    m.prototype.createHinter = function () {
      (this.hinter = document.createElement('div')),
        (this.hinter.className = 'storyblok__hint'),
        this.hideElement(this.hinter),
        document.body.appendChild(this.hinter);
    };
    m.prototype.createHighlighter = function () {
      (this.highlighter = document.createElement('div')),
        (this.highlighter.style.opacity = 0),
        this.hideElement(this.highlighter),
        document.body.appendChild(this.highlighter);
    };
    m.prototype.createOverlay = function () {
      (this.overlay = document.createElement('div')),
        this.overlay.setAttribute('class', d),
        this.overlay.setAttribute('id', d),
        this.hideElement(this.overlay),
        document.body.appendChild(this.overlay);
    };
    m.prototype.calculateElementPosition = function (t, o) {
      if (!o) return void this.hideElement(t);
      var n = e(o);
      if (n) {
        var _a = this.getElementOffset(n),
          e_1 = _a.left,
          o_2 = _a.top;
        o_2 <= 50
          ? this.overlay.classList.add('storyblok__overlay--bottom')
          : this.overlay.classList.remove('storyblok__overlay--bottom'),
          (t.style.top = o_2 + 'px'),
          (t.style.left = e_1 + 'px'),
          (t.style.width = n.offsetWidth + 'px'),
          (t.style.height = n.offsetHeight + 'px'),
          (t.style.minHeight = '5px');
      } else this.hideElement(t);
    };
    m.prototype.getElementOffset = function (e) {
      var t = e.getBoundingClientRect(),
        o = window.pageXOffset || document.documentElement.scrollLeft,
        n = window.pageYOffset || document.documentElement.scrollTop;
      return { top: t.top + n, left: t.left + o };
    };
    m.prototype.resetAllEvents = function () {
      for (var e_2 in this.events) this.events[e_2] = [];
    };
    m.prototype.addMessageListener = function () {
      window.addEventListener &&
        addEventListener('message', this.receiveMessageFromApp.bind(this), !1);
    };
    m.prototype.receiveMessageFromApp = function (e) {
      e && e.data && e.data.action && this.emit(e.data.action, e.data);
    };
    m.prototype.emit = function (e) {
      var t = this.events[e];
      if (e && t && t.length)
        for (var e_3 = 0; e_3 < t.length; e_3++)
          t[e_3].apply(this, [].slice.call(arguments, 1));
    };
    m.prototype.outlineOnMoveHandler = function () {
      var _this = this;
      document.body.classList.add(l),
        this.outlineOnMoveInterval && clearTimeout(this.outlineOnMoveInterval),
        (this.outlineOnMoveInterval = setTimeout(function () {
          document.body.classList.remove(l),
            _this.hinter && (_this.hinter.style.opacity = 0);
        }, 800));
    };
    m.prototype.outlineOnMove = function () {
      document.addEventListener(
        'mousemove',
        this.outlineOnMoveHandler.bind(this),
        !1,
      );
    };
    m.prototype.on = function (e, t) {
      if (e.constructor !== Array) this.subscribeEvent(e, t);
      else
        for (var _i = 0, e_4 = e; _i < e_4.length; _i++) {
          var o_3 = e_4[_i];
          this.subscribeEvent(o_3, t);
        }
    };
    m.prototype.subscribeEvent = function (e, t) {
      'input' === e && (this.actionsEnabled = !0),
        -1 === this.events[e].indexOf(t) && this.events[e].push(t);
    };
    m.prototype.pingEditor = function (e) {
      this.isInIframe
        ? this.sendDataToEditor({ action: 'ping' })
        : ((this.inEditor = !1), e(this)),
        this.on('pingBack', this.handlePingBack(e));
    };
    m.prototype.handlePingBack = function (e) {
      var _this = this;
      return function () {
        (_this.inEditor = !0), e(_this);
      };
    };
    m.prototype.handleEditedBlok = function (e) {
      (this.navigationBreadcrumbs = e.breadcrumbs),
        (this.canAddBlocks = e.canAddBlocks || !1),
        (this.canMoveForward = e.canMoveForward || !1),
        this.updateComponentBase(e.blok);
    };
    m.prototype.handleAddMoveBlok = function (t) {
      var o = e(this.storyId + '-' + t.blockId);
      o && this.handleOpenBlok(o);
    };
    m.prototype.handleDeselectBlock = function (e) {
      document.querySelectorAll('[data-blok-focused]').forEach(function (e) {
        return e.removeAttribute('data-blok-focused');
      }),
        this.hideElement(this.overlay);
    };
    m.prototype.handleDuplicatedBlok = function (t) {
      var o = e(this.storyId + '-' + t.blockId);
      this.handleOpenBlok(o);
    };
    m.prototype.handleWindowClick = function (e) {
      this.handleOpenBlok(e.target, e);
    };
    m.prototype.handleOpenBlok = function (e, t) {
      var o = (function (e) {
        var t = (function (e, t) {
          for (; e && 1 === e.nodeType; ) {
            if (e.hasAttribute('data-blok-c')) return e;
            e = e.parentNode;
          }
          return null;
        })(e);
        if (!t) return null;
        var o = t.getAttribute('data-blok-c');
        return JSON.parse(o);
      })(e);
      if (o)
        return (
          t &&
            o.uid !== this.currentUid &&
            (t.preventDefault(), t.stopPropagation()),
          (this.currentUid = o.uid),
          (this.storyId = o.id),
          void this.openBlok(o)
        );
      t && this.toggleFocusElement(e, !0);
    };
    m.prototype.enterEditmode = function (t) {
      var _this = this;
      var n = (function (e) {
        var t = [],
          n = document.createNodeIterator(
            e,
            NodeFilter.SHOW_COMMENT,
            function () {
              return NodeFilter.FILTER_ACCEPT;
            },
            !1,
          );
        var i = {};
        for (; (i = n.nextNode()); )
          if (i.nodeValue.indexOf('#storyblok#') > -1) {
            var e_5 = i.nodeValue.replace('#storyblok#', ''),
              n_1 = i.nextElementSibling || i.nextSibling,
              s_1 = o(e_5);
            s_1 && t.push({ options: s_1, el: n_1 });
          }
        return t;
      })(document.body);
      t && t.componentNames && (this.componentNames = t.componentNames);
      for (var e_6 = 0; e_6 < n.length; e_6++) {
        var t_1 = n[e_6].el,
          o_4 = n[e_6].options;
        t_1 &&
          ((o_4.name = this.componentNames[o_4.name] || o_4.name),
          t_1.setAttribute('data-blok-c', JSON.stringify(o_4)),
          t_1.setAttribute('data-blok-uid', o_4.id + '-' + o_4.uid),
          t_1.offsetHeight < 5 && (t_1.style['min-height'] = '5px'),
          t_1.classList.add('storyblok__outline'));
      }
      this.on('addedBlock', this.handleAddMoveBlok),
        this.on('duplicatedBlock', this.handleDuplicatedBlok),
        this.on('movedBlock', this.handleAddMoveBlok),
        this.on('enterComponent', this.enterComponent),
        this.on('highlightComponent', this.highlightComponent),
        this.on('hoverComponent', this.hoverComponent),
        this.on('editedBlok', this.handleEditedBlok),
        this.on('deselectBlok', this.handleDeselectBlock),
        setTimeout(function () {
          if (t && t.blockId) {
            var o_5 = e(t.storyId + '-' + t.blockId);
            _this.handleOpenBlok(o_5);
          }
        }, 1e3),
        window.addEventListener('click', this.handleWindowClick.bind(this)),
        null !== this.calcInterval && window.clearInterval(this.calcInterval),
        (this.calcInterval = window.setInterval(function () {
          _this.calculateElementPosition(
            _this.overlay,
            _this.storyId + '-' + _this.currentUid,
          );
        }, 300));
    };
    m.prototype.highlightComponent = function (o) {
      (this.highlighter.innerHTML = ''),
        o.componentIds.length > 0
          ? ((this.highlighter.style.display = 'block'),
            (this.highlighter.style.opacity = 1))
          : ((this.highlighter.style.display = 'none'),
            (this.highlighter.style.opacity = 0));
      for (var n_2 = 0; n_2 < o.componentIds.length; n_2++) {
        var i_1 = o.storyId + '-' + o.componentIds[n_2],
          s_2 = e(i_1);
        if (s_2) {
          var e_7 = document.createElement('div');
          e_7.setAttribute('class', 'storyblok__highlight'),
            this.highlighter.appendChild(e_7),
            this.calculateElementPosition(e_7, i_1),
            o.componentId !== o.componentIds[n_2] ||
              t(s_2) ||
              void 0 === s_2.scrollIntoView ||
              s_2.scrollIntoView();
        }
      }
    };
    m.prototype.hoverComponent = function (e) {
      this.calculateElementPosition(
        this.hinter,
        e.storyId + '-' + e.componentId,
      ),
        (this.hinter.style.opacity = 1),
        (this.hinter.style.display = 'block');
    };
    m.prototype.toggleFocusElement = function (e, t) {
      if (t === void 0) {
        t = !1;
      }
      if (!this.overlay.contains(e)) {
        if (
          (this.handleDeselectBlock(), !t && this.storyId === n('_storyblok'))
        )
          return (
            e.setAttribute('data-blok-focused', !0),
            this.showFocusedElement(this.overlay),
            void (this.focusState = !1)
          );
        this.focusState || this.sendDataToEditor({ action: 'noFocus' });
      }
    };
    m.prototype.showFocusedElement = function (e) {
      this.currentUid
        ? (this.showElement(e),
          this.calculateElementPosition(
            e,
            this.storyId + '-' + this.currentUid,
          ))
        : this.hideElement(e);
    };
    m.prototype.handleBlokActions = function (e) {
      this.sendDataToEditor({ action: e, blok: this.lastBreadcrumbItem }),
        this.hideElement(
          'addBlockBefore' === e || 'addBlockAfter' === e
            ? this.actionsMenu
            : this.overlay,
        ),
        ('moveForward' !== e && 'moveBackward' !== e) || (this.focusState = !0);
    };
    m.prototype.hideElement = function (e) {
      e.style.display = 'none';
    };
    m.prototype.showElement = function (e) {
      e.style.display = 'block';
    };
    m.prototype.toggleElement = function (e) {
      'block' !== e.style.display ? this.showElement(e) : this.hideElement(e);
    };
    m.prototype.enterComponent = function (t) {
      var o = e(t.storyId + '-' + t.componentId);
      (this.hinter.style.opacity = 0),
        (this.hinter.style.display = 'none'),
        this.scrollIntoView(o),
        this.handleOpenBlok(o);
    };
    m.prototype.scrollIntoView = function (e) {
      e &&
        (t(e) ||
          void 0 === e.scrollIntoView ||
          e.scrollIntoView({ behavior: 'smooth', block: 'start' }));
    };
    m.prototype.openBlok = function (e) {
      this.sendDataToEditor({ action: 'edit', dataC: e, config: this.config });
    };
    m.prototype.createComponentContext = function () {
      this.createComponentBase(),
        this.createActionsMenu(),
        this.createActionsMenuItems(),
        this.createBreadcrumbsMenu(),
        this.createActionsMenuButton(),
        this.createBreadcrumbsMenuButton(),
        this.createComponentLabel(),
        this.createComponentButtonLabel();
    };
    m.prototype.createComponentBase = function () {
      (this.componentBase = document.createElement('div')),
        this.componentBase.setAttribute('class', c),
        this.componentBase.setAttribute('id', c),
        this.overlay.append(this.componentBase);
    };
    m.prototype.createComponentLabel = function () {
      (this.componentLabel = document.createElement('span')),
        this.componentLabel.setAttribute(
          'class',
          'storyblok__overlay-menu-label',
        ),
        this.componentBase.prepend(this.componentLabel);
    };
    m.prototype.createComponentButtonLabel = function () {
      var e = document.createElement('span');
      e.setAttribute('class', 'storyblok__overlay-menu-label'),
        this.breadcrumbsButtonMenu.appendChild(e);
    };
    m.prototype.updateComponentLabel = function (e) {
      document
        .querySelectorAll('.storyblok__overlay-menu-label')
        .forEach(function (t) {
          return (t.innerText = i(e));
        });
    };
    m.prototype.updateComponentBase = function (t) {
      var _this = this;
      this.hideElement(this.actionsMenu),
        this.hideElement(this.breadcrumbsMenu),
        this.overlay.classList.add('storyblok__overlay--clicked'),
        setTimeout(function () {
          _this.overlay.classList.remove('storyblok__overlay--clicked');
        }, 400),
        this.navigationBreadcrumbs.length > 1
          ? (this.updateBreadcrumbsMenu(this.navigationBreadcrumbs),
            this.actionsEnabled
              ? this.updateActionsMenu()
              : (this.componentBase.setAttribute(
                  'class',
                  'storyblok__overlay-menu storyblok__overlay-menu--simple',
                ),
                this.hideElement(this.actionsMenuButton)),
            this.hideElement(this.componentLabel))
          : (this.hideElement(this.breadcrumbsButtonMenu),
            this.hideElement(this.actionsMenuButton),
            this.showElement(this.componentLabel)),
        this.updateComponentLabel(t);
      var o = e(this.storyId + '-' + t.uid);
      o && this.toggleFocusElement(o);
    };
    m.prototype.createBreadcrumbsMenuButton = function () {
      var _this = this;
      (this.breadcrumbsButtonMenu = document.createElement('button')),
        this.breadcrumbsButtonMenu.setAttribute(
          'class',
          'storyblok__overlay-menu-btn storyblok__overlay-menu-btn-parent',
        ),
        this.breadcrumbsButtonMenu.prepend(
          s({
            viewBox: '0 0 24 24',
            svgClass: 'storyblok__overlay-menu-svg',
            path: 'M13.73 14.284l-2.197-2.216 2.197-2.217a1.051 1.051 0 000-1.477 1.03 1.03 0 00-1.465 0l-2.93 2.955a1.043 1.043 0 00-.287.554l-.014.123v.123c.014.247.115.489.301.677l2.93 2.956a1.03 1.03 0 001.465 0 1.051 1.051 0 000-1.478z',
            pathFill: '#ffffff',
          }),
        ),
        this.breadcrumbsButtonMenu.addEventListener('click', function () {
          _this.toggleElement(_this.breadcrumbsMenu),
            _this.hideElement(_this.actionsMenu);
        }),
        this.componentBase.prepend(this.breadcrumbsButtonMenu);
    };
    m.prototype.createBreadcrumbsMenu = function () {
      (this.breadcrumbsMenu = document.createElement('div')),
        this.breadcrumbsMenu.setAttribute('class', u),
        this.breadcrumbsMenu.setAttribute('id', u),
        this.componentBase.append(this.breadcrumbsMenu);
    };
    m.prototype.updateBreadcrumbsMenu = function (e) {
      var _this = this;
      this.breadcrumbsMenu.innerHTML = '';
      var t = [];
      var _loop_1 = function (o_6) {
        (t[o_6] = document.createElement('button')),
          (t[o_6].innerHTML = i({ name: e[o_6].component }));
        var n_3 = e[o_6];
        t[o_6].addEventListener('click', function (e) {
          e.stopPropagation(),
            (_this.currentUid = n_3._uid),
            _this.openBlok({
              id: _this.storyId,
              uid: n_3._uid,
              name: n_3.component,
            });
        }),
          t[o_6].classList.add('storyblok__breadcrumbs-menu__menu-item'),
          e[o_6]._uid === this_1.currentUid &&
            (t[o_6].classList.add(
              'storyblok__breadcrumbs-menu__menu-item--selected',
            ),
            t[o_6].setAttribute('disabled', !0)),
          this_1.breadcrumbsMenu.appendChild(t[o_6]),
          (this_1.breadcrumbsButtonMenu.style.display = 'flex');
      };
      var this_1 = this;
      for (var o_6 = 0; o_6 < e.length; o_6++) {
        _loop_1(o_6);
      }
    };
    m.prototype.createActionsMenuButton = function () {
      var _this = this;
      (this.actionsMenuButton = document.createElement('button')),
        this.actionsMenuButton.setAttribute(
          'class',
          'storyblok__overlay-menu-btn storyblok__overlay-menu-btn-action',
        ),
        this.actionsMenuButton.prepend(
          s({
            viewBox: '0 0 24 24',
            svgClass: 'storyblok__overlay-menu-svg',
            path: 'M7.5 11a1.5 1.5 0 110 3 1.5 1.5 0 010-3zm10 0a1.5 1.5 0 110 3 1.5 1.5 0 010-3zm-5 0a1.5 1.5 0 110 3 1.5 1.5 0 010-3z',
            pathFill: '#ffffff',
          }),
        ),
        this.actionsMenuButton.addEventListener('click', function () {
          _this.toggleElement(_this.actionsMenu),
            _this.breadcrumbsMenu && _this.hideElement(_this.breadcrumbsMenu);
        }),
        this.componentBase.append(this.actionsMenuButton);
    };
    m.prototype.createActionsMenuItems = function () {
      var _this = this;
      this.actionsMenuItems = [
        {
          eventFunction: function () {
            return _this.handleBlokActions.bind(_this, 'addBlockBefore');
          },
          innerHTML: 'Add Block Before',
          order: 0,
          show: function () {
            return _this.canAddBlocks;
          },
        },
        {
          eventFunction: function () {
            return _this.handleBlokActions.bind(_this, 'addBlockAfter');
          },
          innerHTML: 'Add Block After',
          order: 1,
          show: function () {
            return _this.canAddBlocks;
          },
        },
        {
          eventFunction: function () {
            return _this.handleBlokActions.bind(_this, 'duplicateBlock');
          },
          innerHTML: 'Duplicate',
          order: 2,
          show: function () {
            return _this.canAddBlocks;
          },
        },
        {
          innerHTML: 'Copy',
          order: 3,
          show: function () {
            return !1;
          },
        },
        {
          separator: !0,
          order: 4,
          show: function () {
            return (
              _this.canAddBlocks &&
              !(!_this.canMoveForward && !_this.lastBreadcrumbItem._parentindex)
            );
          },
        },
        {
          eventFunction: function () {
            return _this.handleBlokActions.bind(_this, 'moveForward');
          },
          innerHTML: 'Move Forward',
          order: 5,
          show: function () {
            return _this.canMoveForward;
          },
        },
        {
          eventFunction: function () {
            return _this.handleBlokActions.bind(_this, 'moveBackward');
          },
          innerHTML: 'Move Backward',
          order: 6,
          show: function () {
            return _this.lastBreadcrumbItem._parentindex;
          },
        },
        {
          separator: !0,
          order: 7,
          show: function () {
            return _this.canAddBlocks;
          },
        },
        {
          eventFunction: function () {
            return _this.handleBlokActions.bind(_this, 'deleteBlock');
          },
          className: 'storyblok__actions-menu__menu-item--delete',
          innerHTML: 'Delete',
          order: 8,
          show: function () {
            return !0;
          },
        },
      ];
    };
    m.prototype.createActionsMenu = function () {
      (this.actionsMenu = document.createElement('div')),
        this.actionsMenu.setAttribute('class', h),
        this.actionsMenu.setAttribute('id', h),
        this.componentBase.append(this.actionsMenu);
    };
    m.prototype.updateActionsMenu = function () {
      var _this = this;
      (this.actionsMenu.innerHTML = ''),
        this.actionsMenuItems
          .sort(function (e, t) {
            return e.order > t.order ? 1 : t.order > e.order ? -1 : 0;
          })
          .forEach(function (e) {
            if (e.separator && e.show())
              return void _this.actionsMenu.appendChild(
                document.createElement('hr'),
              );
            var t = document.createElement(e.element ? e.element : 'button');
            t.classList.add('storyblok__actions-menu__menu-item'),
              e.className && t.classList.add(e.className),
              (t.innerHTML = e.innerHTML),
              e.eventFunction &&
                t.addEventListener(
                  e.event ? e.event : 'click',
                  e.eventFunction(),
                ),
              e.show() && _this.actionsMenu.appendChild(t);
          }),
        (this.actionsMenuButton.style.display = 'flex');
    };
    return m;
  })();
  void 0 !== window &&
    (window.StoryblokBridge = /** @class */ (function () {
      function StoryblokBridge(e) {
        var t = new m(e);
        (this.enterEditmode = function (e) {
          t.enterEditmode(e);
        }),
          (this.isInEditor = function () {
            return t.isInEditor();
          }),
          (this.on = function (e, o) {
            t.on(e, o);
          }),
          (this.pingEditor = function (e) {
            t.pingEditor(e);
          });
      }
      return StoryblokBridge;
    })());
});
//# sourceMappingURL=storyblok-v2-latest.js.map
