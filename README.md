# ngx-storyblok

A library for easy integration of [Storyblok headless CMS](https://www.storyblok.com) into Angular.

## Installation

```shell
ng add ngx-storyblok
```

## Badges

[![pipeline status](https://gitlab.com/masterfix/ngx-storyblok/badges/main/pipeline.svg)](https://gitlab.com/masterfix/ngx-storyblok/-/commits/main)

[![coverage report](https://gitlab.com/masterfix/ngx-storyblok/badges/main/coverage.svg)](https://gitlab.com/masterfix/ngx-storyblok/-/commits/main)

[![npm](https://img.shields.io/npm/v/ngx-storyblok)](https://www.npmjs.com/package/ngx-storyblok)

[![npm](https://img.shields.io/npm/dw/ngx-storyblok)](https://www.npmjs.com/package/ngx-storyblok)

[![npm bundle size](https://img.shields.io/bundlephobia/min/ngx-storyblok)](https://www.npmjs.com/package/ngx-storyblok)

[![angular](https://badges.aleen42.com/src/angular.svg)](https://angular.io)

[![typescript](https://badges.aleen42.com/src/typescript.svg)](https://www.typescriptlang.org)

[![semantic-release](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg)](https://github.com/semantic-release/semantic-release)

[![MIT License](https://img.shields.io/apm/l/atomic-design-ui.svg)](LICENSE)

## Authors

- [@masterfix](https://gitlab.com/masterfix)

## License

[MIT](https://choosealicense.com/licenses/mit/)
